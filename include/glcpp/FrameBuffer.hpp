#pragma once

#include <memory>

#include <glcpp/Texture2D.hpp>

#include "../../detail/contracts.hpp"
#include "../../detail/opengl.hpp"

namespace glcpp
{

class FrameBuffer
{
public:
	explicit FrameBuffer();
	FrameBuffer(const FrameBuffer&) = delete;
	FrameBuffer(FrameBuffer&&) noexcept = delete;
	~FrameBuffer();

	FrameBuffer& operator=(const FrameBuffer&) = delete;
	FrameBuffer& operator=(FrameBuffer&&) noexcept = delete;

	void attach(std::shared_ptr<Texture2D> tex2d);

	template<typename TFunc>
	void bind(TFunc&& procedure)
	{
		GLint viewportSize[4];
		glGetIntegerv(GL_VIEWPORT, viewportSize);
		checkGl();

		GLint prevFramebuffer;
		glGetIntegerv(GL_FRAMEBUFFER_BINDING, &prevFramebuffer);
		checkGl();

		glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffer);
		checkGl();

		glViewport(0, 0, width(), height());
		checkGl();

		procedure();

		glViewport(viewportSize[0], viewportSize[1], viewportSize[2], viewportSize[3]);
		checkGl();

		glBindFramebuffer(GL_FRAMEBUFFER, prevFramebuffer);
		checkGl();
	}
	GLsizei width() const noexcept;
	GLsizei height() const noexcept;
private:
	GLuint m_framebuffer{ 0 };
	std::shared_ptr<Texture2D> m_colorAttachment0;
};

}