#pragma once

#include "../../detail/contracts.hpp"
#include "../../detail/opengl.hpp"

namespace glcpp
{

class PixelBufferObject
{
public:
	explicit PixelBufferObject();
	PixelBufferObject(const PixelBufferObject&) = delete;
	PixelBufferObject(PixelBufferObject&&) noexcept = delete;
	~PixelBufferObject();

	PixelBufferObject& operator=(const PixelBufferObject&) = delete;
	PixelBufferObject& operator=(PixelBufferObject&&) noexcept = delete;

	void allocateToPack(GLsizeiptr size, GLenum bufferUsage);

	template<typename TFunc>
	void bindToPack(TFunc&& procedure);
private:
	GLsizeiptr m_size{ 0 };
	GLuint m_pboBuffer{ 0 };
};

template<typename TFunc>
void PixelBufferObject::bindToPack(TFunc&& procedure)
{
	GLint prevBuffer;
	glGetIntegerv(GL_PIXEL_PACK_BUFFER_BINDING, &prevBuffer);
	checkGl();

	glBindBuffer(GL_PIXEL_PACK_BUFFER, m_pboBuffer);
	checkGl();

	GLint pack;
	glGetIntegerv(GL_PACK_ALIGNMENT, &pack);
	checkGl();

	procedure();

	glPixelStorei(GL_PACK_ALIGNMENT, pack);
	checkGl();

	glBindBuffer(GL_PIXEL_PACK_BUFFER, prevBuffer);
	checkGl();
}

}