#pragma once

#include <memory>

#include "../../detail/ShaderBase.hpp"

namespace glcpp {

class ComputeShader : public ShaderBase
{
public:
	static std::unique_ptr<ComputeShader> create(const GLchar* code);
protected:
	explicit ComputeShader(const GLchar* code);
};

}