#pragma once

#include <memory>
#include <string>
#include <vector>
#include <utility>

#include "FragmentShader.hpp"
#include "VertexShader.hpp"
#include "ComputeShader.hpp"

#include "../../detail/contracts.hpp"

namespace glcpp
{

//a RAII-wrapper over m_program
class ShaderProgramBase
{
public:
	explicit ShaderProgramBase();
	ShaderProgramBase(const ShaderProgramBase&) = delete;
	ShaderProgramBase(ShaderProgramBase&& orig) noexcept;
	~ShaderProgramBase();

	ShaderProgramBase& operator=(const ShaderProgramBase&) = delete;
	ShaderProgramBase& operator=(ShaderProgramBase&& rhs) noexcept;

protected:
	GLuint m_program{ 0 };
};

class ShaderProgram : public ShaderProgramBase
{
public:
	explicit ShaderProgram(std::shared_ptr<VertexShader> vs, std::shared_ptr<FragmentShader> fs);
	explicit ShaderProgram(std::unique_ptr<ComputeShader> cs);

	ShaderProgram(const ShaderProgram&) = delete;
	ShaderProgram(ShaderProgram&&) noexcept = default;
	~ShaderProgram() = default;

	ShaderProgram& operator=(const ShaderProgram&) = delete;
	ShaderProgram& operator=(ShaderProgram&&) noexcept = default;

	const std::string& linkLog() const noexcept { return m_linkLog; }
	const std::string& validationLog() const noexcept { return m_validationLog; }
	[[nodiscard]] GLint uniformLoc(std::string name) const noexcept;
	[[nodiscard]] GLint attribLoc(std::string name) const noexcept;
	void setUniform(std::string name, GLuint scalar) noexcept;
	void setUniform(std::string name, GLint scalar) noexcept;
	void setUniform(std::string name, GLfloat scalar) noexcept;
	void setUniform(std::string name, GLfloat x, GLfloat y) noexcept;
	void setUniform(std::string name, GLfloat x, GLfloat y, GLfloat z) noexcept;
	void setUniformMat4(std::string name, GLfloat* mat4x4) noexcept;

	template <typename TFunc> void bind(TFunc&& func) const noexcept;

private:
	std::string m_linkLog;
	std::string m_validationLog;
	std::vector<std::shared_ptr<ShaderBase>> m_shaders;

	mutable std::vector<std::pair<std::string, GLint>> m_uniformLocations;
	mutable std::vector<std::pair<std::string, GLint>> m_attributeLocations;

	template<typename ...TShaders>
	void attachAndCompile(TShaders... shaders);
	void attach(const std::shared_ptr<VertexShader>& vs, const std::shared_ptr<FragmentShader>& fs);
	void attach(const std::unique_ptr<ComputeShader>& cs);
};

template <typename TFunc>
void ShaderProgram::bind(TFunc&& func) const noexcept
{
	GLint prevProgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &prevProgram);
	checkGl();

	glUseProgram(m_program);
	checkGl();

	func();

	glUseProgram(prevProgram);
	checkGl();
}

}