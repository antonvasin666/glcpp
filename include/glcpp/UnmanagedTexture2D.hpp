#pragma once

#include "../../detail/contracts.hpp"
#include "../../detail/opengl.hpp"

namespace glcpp
{

class UnmanagedTexture2D
{
public:
	explicit UnmanagedTexture2D() = default;
	explicit UnmanagedTexture2D(GLuint texture, GLsizei width, GLsizei height, GLenum internalFormat) noexcept :
	m_texture(texture), m_width(width), m_height(height), m_internalFormat(internalFormat)
	{
	}
	UnmanagedTexture2D(const UnmanagedTexture2D&) = default;
	UnmanagedTexture2D(UnmanagedTexture2D&&) noexcept = default;
	~UnmanagedTexture2D() = default;

	UnmanagedTexture2D& operator=(const UnmanagedTexture2D&) = default;
	UnmanagedTexture2D& operator=(UnmanagedTexture2D&&) noexcept = default;

	template <typename TFunc> void bind(GLuint slot, TFunc&& procedure) const noexcept;
	template <typename TFunc> void bindImage(GLuint slot, GLint level, GLboolean layered, GLint layer, GLenum access, TFunc&& procedure);
	[[nodiscard]] GLuint nativeHandle() const noexcept { return m_texture; }
	[[nodiscard]] GLsizei width() const noexcept { return m_width; }
	[[nodiscard]] GLsizei height() const noexcept { return m_height; }
protected:
	GLuint m_texture{ 0 };
	GLsizei m_width{ 0 };
	GLsizei m_height{ 0 };
	GLenum m_internalFormat { 0 };
};

template <typename TFunc>
void UnmanagedTexture2D::bind(GLuint slot, TFunc&& procedure) const noexcept
{
	GLint lastSlot;
	glGetIntegerv(GL_ACTIVE_TEXTURE, &lastSlot);
	checkGl();

	glActiveTexture(GL_TEXTURE0 + slot);
	checkGl();

	GLint lastTexture;
	glGetIntegerv(GL_TEXTURE_BINDING_2D, &lastTexture);
	checkGl();

	glBindTexture(GL_TEXTURE_2D, m_texture);
	checkGl();

	GLint unpack;
	glGetIntegerv(GL_UNPACK_ALIGNMENT, &unpack);
	checkGl();

	procedure();

	glPixelStorei(GL_UNPACK_ALIGNMENT, unpack);
	checkGl();

	glBindTexture(GL_TEXTURE_2D, lastTexture);
	checkGl();

	glActiveTexture(lastSlot);
	checkGl();
}

template <typename TFunc>
void UnmanagedTexture2D::bindImage(GLuint slot, GLint level, GLboolean layered, GLint layer, GLenum access, TFunc&& procedure)
{
//*******************
//**Samsung Mali GPU sets GL_INVALID_ENUM error after all glGet* calls
//*******************
//	GLint prevBindingName;
//	GLint prevBindingLevel;
//	GLboolean prevBindingLayered;
//	GLint prevBindingLayer;
//	GLint prevBindingAccess;
//	GLint prevBindingFormat;
//
//	glGetIntegerv(GL_IMAGE_BINDING_NAME, &prevBindingName);
//	checkGl();
//	glGetIntegerv(GL_IMAGE_BINDING_LEVEL, &prevBindingLevel);
//	checkGl();
//	glGetBooleanv(GL_IMAGE_BINDING_LAYERED, &prevBindingLayered);
//	checkGl();
//	glGetIntegerv(GL_IMAGE_BINDING_LAYER, &prevBindingLayer);
//	checkGl();
//	glGetIntegerv(GL_IMAGE_BINDING_ACCESS, &prevBindingAccess);
//	checkGl();
//	glGetIntegerv(GL_IMAGE_BINDING_FORMAT, &prevBindingFormat);
//	checkGl();

//	glBindImageTexture is a very capricious function
//	Some, but not all error reasons are covered here:
//	https://www.khronos.org/registry/OpenGL-Refpages/es3.1/html/glBindImageTexture.xhtml
//
//	glBindImageTexture also returns GL_INVALID_VALUE if internal format is not found in table 8.27
//	https://www.khronos.org/registry/OpenGL/specs/es/3.1/es_spec_3.1.pdf
	glBindImageTexture(slot, m_texture, level, layered, layer, access, m_internalFormat);
	checkGl();

	procedure();

//*******************
//	glBindImageTexture(slot, prevBindingName, prevBindingLevel, prevBindingLayered, prevBindingLayer, GLenum(prevBindingAccess), GLenum(prevBindingFormat));
//	checkGl();
}

}