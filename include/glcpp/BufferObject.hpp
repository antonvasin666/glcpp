#pragma once

#include <vector>

#include "../../detail/contracts.hpp"
#include "../../detail/opengl.hpp"

namespace glcpp
{

template <typename T, GLenum defaultTarget, GLenum getIntegerBinding>
class BufferObject
{
public:
    constexpr static GLenum kBufferTarget = defaultTarget;
    constexpr static GLenum kGetIntegerBinding = getIntegerBinding;

    explicit BufferObject();
    explicit BufferObject(const std::vector<T>& data);
    explicit BufferObject(GLsizeiptr size);
    BufferObject(const BufferObject&) = delete;
    BufferObject(BufferObject&&) noexcept = delete;
    ~BufferObject();

    BufferObject& operator=(const BufferObject&) = delete;
    BufferObject& operator=(BufferObject&&) noexcept = delete;

    void resize(GLsizeiptr size, GLenum usage = GL_STREAM_DRAW, const void* data = nullptr);
    GLsizeiptr size() const noexcept { return m_size; }

    void swap(BufferObject& other) noexcept;

    GLuint nativeHandle() const noexcept { return m_buffer; }

    template<typename TFunc>
    void bind(GLuint slot, TFunc&& procedure, GLenum target = defaultTarget, GLenum getBinding = getIntegerBinding) const noexcept;
    template<typename TFunc>
    void bind(TFunc&& procedure, GLenum target = defaultTarget, GLenum getBinding = getIntegerBinding) const noexcept;

    template<typename TFunc>
    void map(GLenum access, TFunc&& procedure);

    template<typename TFunc>
    void map(TFunc&& procedure) const;

    template<typename TFunc>
    void map(TFunc&& procedure, GLintptr offsetIdx, GLsizeiptr elementsCount) const;

private:
    GLuint m_buffer{ 0 };
    GLsizeiptr m_size{ 0 };
    GLsizeiptr m_capacity { 0 };
};

template <typename T, GLenum defaultTarget, GLenum getIntegerBinding>
BufferObject<T, defaultTarget, getIntegerBinding>::BufferObject()
{
    glGenBuffers(1, &m_buffer);
    checkGl();
}

template <typename T, GLenum defaultTarget, GLenum getIntegerBinding>
BufferObject<T, defaultTarget, getIntegerBinding>::BufferObject(const std::vector<T>& data) : BufferObject()
{
    resize(data.size(), GL_STREAM_DRAW, data.data());
}

template <typename T, GLenum defaultTarget, GLenum getIntegerBinding>
BufferObject<T, defaultTarget, getIntegerBinding>::BufferObject(GLsizeiptr size) : BufferObject()
{
    resize(size, GL_STREAM_DRAW, nullptr);
}

template <typename T, GLenum defaultTarget, GLenum getIntegerBinding>
BufferObject<T, defaultTarget, getIntegerBinding>::~BufferObject()
{
    glDeleteBuffers(1, &m_buffer);
    checkGl();
}

template <typename T, GLenum defaultTarget, GLenum getIntegerBinding>
void BufferObject<T, defaultTarget, getIntegerBinding>::resize(GLsizeiptr size, GLenum usage, const void* data)
{
    if (size <= m_capacity && data == nullptr)
    {
        m_size = size;
        return;
    }

    bind([&]
    {
        m_capacity = size;
        m_size = size;

        glBufferData(defaultTarget, m_capacity * sizeof(T), data, usage);
        checkGl();
    });
}

template <typename T, GLenum defaultTarget, GLenum getIntegerBinding>
void BufferObject<T, defaultTarget, getIntegerBinding>::swap(BufferObject<T, defaultTarget, getIntegerBinding>& other) noexcept
{
    std::swap(m_buffer, other.m_buffer);
    std::swap(m_capacity, other.m_capacity);
    std::swap(m_size, other.m_size);
}

template <typename T, GLenum defaultTarget, GLenum getIntegerBinding>
template<typename TFunc>
void BufferObject<T, defaultTarget, getIntegerBinding>::bind(GLuint slot, TFunc&& procedure, GLenum target, GLenum getBinding) const noexcept
{
    GLint prevBuffer;
    glGetIntegeri_v(getBinding, slot, &prevBuffer);
    checkGl();

    glBindBufferBase(target, slot, m_buffer);
    checkGl();

    procedure();

    glBindBufferBase(target, slot, prevBuffer);
    checkGl();
}

template <typename T, GLenum defaultTarget, GLenum getIntegerBinding>
template<typename TFunc>
void BufferObject<T, defaultTarget, getIntegerBinding>::bind(TFunc&& procedure, GLenum target, GLenum getBinding) const noexcept
{
    GLint prevBuffer;
    glGetIntegerv(getBinding, &prevBuffer);
    checkGl();

    glBindBuffer(target,  m_buffer);
    checkGl();

    procedure();

    glBindBuffer(target,  prevBuffer);
    checkGl();
}

template <typename T, GLenum defaultTarget, GLenum getIntegerBinding>
template<typename TFunc>
void BufferObject<T, defaultTarget, getIntegerBinding>::map(GLenum access, TFunc&& procedure)
{
    bind([&]
    {
        void* rawData = glMapBufferRange(defaultTarget, 0, m_size * sizeof(T), access);
        checkGl();

        procedure(reinterpret_cast<T*>(rawData));

        glUnmapBuffer(defaultTarget);
        checkGl();
    });
}

template <typename T, GLenum defaultTarget, GLenum getIntegerBinding>
template<typename TFunc>
void BufferObject<T, defaultTarget, getIntegerBinding>::map(TFunc&& procedure) const
{
    bind([&]
    {
        void* rawData = glMapBufferRange(defaultTarget, 0, m_size * sizeof(T), GL_MAP_READ_BIT);
        checkGl();

        procedure(reinterpret_cast<const T*>(rawData));

        glUnmapBuffer(defaultTarget);
        checkGl();
    });
}

template <typename T, GLenum defaultTarget, GLenum getIntegerBinding>
template<typename TFunc>
void BufferObject<T, defaultTarget, getIntegerBinding>::map(TFunc&& procedure, GLintptr offsetIdx, GLsizeiptr elementsCount) const
{
    bind([&]
    {
        void* rawData = glMapBufferRange(defaultTarget, offsetIdx * sizeof(T), elementsCount * sizeof(T), GL_MAP_READ_BIT);
        checkGl();

        procedure(reinterpret_cast<T*>(rawData));

        glUnmapBuffer(defaultTarget);
        checkGl();
    });
}

}