#pragma once

#include <optional>
#include <glcpp/UnmanagedTexture2D.hpp>

namespace glcpp
{

class Texture2D : public UnmanagedTexture2D
{
public:
	explicit Texture2D() noexcept;
	~Texture2D();

	struct TextureData
	{
		GLenum format;
		GLenum type;
		GLint alignment;
		void* data;
	};
	void allocate(GLsizei width, GLsizei height, GLint internalFormat, std::optional<TextureData> texture = {});
private:
};

}