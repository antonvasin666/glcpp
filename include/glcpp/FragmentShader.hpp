#pragma once

#include <memory>

#include "../../detail/ShaderBase.hpp"

namespace glcpp
{

class FragmentShader : public ShaderBase
{
public:
	static std::shared_ptr<FragmentShader> create(const GLchar* code);
protected:
	explicit FragmentShader(const GLchar* code);
};

}