#pragma once

#include "../../detail/contracts.hpp"
#include "../../detail/opengl.hpp"

namespace glcpp::ScreenSpaceQuad
{

void draw(GLuint positionAttributeLocation);

template<typename TFunc> void disableVAO(TFunc&& procedure)
{
	GLint lastVertexArray;
	glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &lastVertexArray);
	checkGl();

	glBindVertexArray(0);
	checkGl();

	procedure();

	glBindVertexArray(lastVertexArray);
	checkGl();
}

}