#pragma once

#include <memory>

#include "../../detail/ShaderBase.hpp"

namespace glcpp 
{

class VertexShader : public ShaderBase
{
public:
	static std::shared_ptr<VertexShader> create(const GLchar* code);
protected:
	explicit VertexShader(const GLchar* code);
};

}