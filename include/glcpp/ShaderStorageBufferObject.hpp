#pragma once

#include <vector>

#include "../../detail/contracts.hpp"
#include "../../detail/opengl.hpp"
#include "BufferObject.hpp"

namespace glcpp
{
template <typename T>
using ShaderStorageBufferObject = BufferObject<T, GL_SHADER_STORAGE_BUFFER, GL_SHADER_STORAGE_BUFFER_BINDING>;
}