#pragma once

#include "BufferObject.hpp"

#include "../../detail/contracts.hpp"
#include "../../detail/opengl.hpp"

namespace glcpp
{

class VertexArrayObject
{
public:
    explicit VertexArrayObject();
    VertexArrayObject(const VertexArrayObject&) = delete;
    VertexArrayObject(VertexArrayObject&&) noexcept = delete;
    ~VertexArrayObject();

    VertexArrayObject& operator= (const VertexArrayObject&) = delete;
    VertexArrayObject& operator= (VertexArrayObject&&) = delete;

    template<typename TFunc>
    void bind(TFunc&& procedure);
private:
    GLuint m_vao {0u};
};

template<typename TFunc>
void VertexArrayObject::bind(TFunc&& procedure)
{
    GLint lastVertexArray;
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &lastVertexArray);
    checkGl();

    glBindVertexArray(m_vao);
    checkGl();

    procedure();

    glBindVertexArray(lastVertexArray);
    checkGl();
}

}