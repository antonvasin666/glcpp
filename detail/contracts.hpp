#pragma once

#include <cassert>

#include <exception>
#include <stdexcept>

#include "opengl.hpp"

namespace glcpp
{

#if defined(DEBUG) || defined(_DEBUG)
    inline void ensure(bool condition)
    {
        assert(condition);
    }
    inline void checkGl()
    {
        const auto status = glGetError();
        ensure(status == GL_NO_ERROR);
    }
#else
    inline void ensure (bool condition)
    {
        if (!condition)
        {
            std::terminate();
        }
    }
    inline void checkGl()
    {
        //const auto status = glGetError();
        //ensure(status == GL_NO_ERROR);
    }
#endif
}