#include <glcpp/VertexShader.hpp>

#include "contracts.hpp"

namespace glcpp
{

std::shared_ptr<VertexShader> glcpp::VertexShader::create(const GLchar* code)
{
	class VertexShaderWrapper : public VertexShader
	{
	public:
		explicit VertexShaderWrapper(const GLchar* code) : VertexShader(code) {}
	};
	return std::make_shared<VertexShaderWrapper>(code);
}

VertexShader::VertexShader(const GLchar* code) : ShaderBase(GL_VERTEX_SHADER)
{
	const auto& log = ShaderBase::compile(code);
	ensure(log.empty());
}

}