#include <glcpp/ScreenSpaceQuad.hpp>

static float squareCoords[] = {
		0.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
};
static short indices[] = { 0, 1, 2, 0, 2, 3 };

void glcpp::ScreenSpaceQuad::draw(GLuint positionAttributeLocation)
{
	glEnableVertexAttribArray(positionAttributeLocation);
	checkGl();

	glVertexAttribPointer(positionAttributeLocation, 3, GL_FLOAT, false, 3 * 4, squareCoords);
	checkGl();

	glDrawElements(GL_TRIANGLES, 3 * 2, GL_UNSIGNED_SHORT, indices);
	checkGl();

	glDisableVertexAttribArray(positionAttributeLocation);
	checkGl();
}
