#include <glcpp/VertexArrayObject.hpp>

glcpp::VertexArrayObject::VertexArrayObject()
{
    glGenVertexArrays(1, &m_vao);
    glcpp::checkGl();
}

glcpp::VertexArrayObject::~VertexArrayObject()
{
    glDeleteVertexArrays(1, &m_vao);
    glcpp::checkGl();
}

