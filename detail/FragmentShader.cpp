#include <glcpp/FragmentShader.hpp>

#include "contracts.hpp"

namespace glcpp
{

std::shared_ptr<FragmentShader> glcpp::FragmentShader::create(const GLchar* code)
{
	class FragmentShaderWrapper : public FragmentShader
	{
	public:
		explicit FragmentShaderWrapper(const GLchar* code) : FragmentShader(code) {}
	};
	return std::make_shared<FragmentShaderWrapper>(code);
}

FragmentShader::FragmentShader(const GLchar* code) : ShaderBase(GL_FRAGMENT_SHADER)
{
	const auto log = ShaderBase::compile(code);
	ensure(log.empty());
}

}