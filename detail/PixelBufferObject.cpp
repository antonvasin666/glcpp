#include <glcpp/PixelBufferObject.hpp>

namespace glcpp
{

PixelBufferObject::PixelBufferObject()
{
	glGenBuffers(1, &m_pboBuffer);
	checkGl();
}

PixelBufferObject::~PixelBufferObject()
{
	glDeleteBuffers(1, &m_pboBuffer);
	checkGl();
}

void PixelBufferObject::allocateToPack(GLsizeiptr size, GLenum bufferUsage)
{
	if (m_size == size)
		return;

	bindToPack([&]
	{
		glBufferData(GL_PIXEL_PACK_BUFFER, size, nullptr, bufferUsage);
		checkGl();
	});

	m_size = size;
}

}