#include <glcpp/FragmentShader.hpp>
#include <glcpp/FrameBuffer.hpp>


namespace glcpp
{

FrameBuffer::FrameBuffer()
{
	glGenFramebuffers(1, &m_framebuffer);
	checkGl();
}

FrameBuffer::~FrameBuffer()
{
	glDeleteBuffers(1, &m_framebuffer);
	checkGl();
}

void FrameBuffer::attach(std::shared_ptr<Texture2D> tex2d)
{
	if (m_colorAttachment0 == tex2d)
		return;

	bind([&]
	{
		GLuint textureHandle = tex2d ? tex2d->nativeHandle() : 0;

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
			textureHandle, 0);
		checkGl();
		m_colorAttachment0 = std::move(tex2d);
	});
}

GLsizei FrameBuffer::width() const noexcept
{
	if (m_colorAttachment0 == nullptr)
	{
		return 0;
	}
	return m_colorAttachment0->width();
}

GLsizei FrameBuffer::height() const noexcept
{
	if (m_colorAttachment0 == nullptr)
	{
		return 0;
	}
	return m_colorAttachment0->height();
}

}