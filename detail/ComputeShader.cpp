#include <glcpp/ComputeShader.hpp>

#include "contracts.hpp"

namespace glcpp
{

std::unique_ptr<ComputeShader> glcpp::ComputeShader::create(const GLchar* code)
{
	class ComputeShaderWrapper : public ComputeShader
	{
	public:
		explicit ComputeShaderWrapper(const GLchar* code) : ComputeShader(code) {}
	};
	return std::make_unique<ComputeShaderWrapper>(code);
}

ComputeShader::ComputeShader(const GLchar* code) : ShaderBase(GL_COMPUTE_SHADER)
{
	const auto log = ShaderBase::compile(code);
	ensure(log.empty());
}

}