#pragma once

#ifdef USE_WINDOWS_GL
#include <gl/glew.h>
#else
#include <GLES3/gl3.h>
#include <GLES3/gl31.h>
#endif