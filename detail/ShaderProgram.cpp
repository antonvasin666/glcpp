#ifdef _DEBUG
	#if defined(_WINDOWS)
		#include <Windows.h>
	#elif defined(ANDROID)
		#include <android/log.h>
	#endif
#endif

#include <glcpp/ShaderProgram.hpp>

#include <vector>

#include "contracts.hpp"

namespace glcpp
{

ShaderProgramBase::ShaderProgramBase()
{
	m_program = glCreateProgram();
	checkGl();
}

ShaderProgramBase::ShaderProgramBase(ShaderProgramBase&& orig) noexcept
{
	std::swap(orig.m_program, m_program);
}

ShaderProgramBase::~ShaderProgramBase()
{
	glDeleteProgram(m_program);
	checkGl();
}

ShaderProgramBase& ShaderProgramBase::operator=(ShaderProgramBase&& rhs) noexcept
{
	std::swap(rhs.m_program, m_program);
	return *this;
}

ShaderProgram::ShaderProgram(std::shared_ptr<VertexShader> vs, std::shared_ptr<FragmentShader> fs)
{
	attachAndCompile(std::move(vs), std::move(fs));
}
ShaderProgram::ShaderProgram(std::unique_ptr<ComputeShader> cs)
{
	attachAndCompile(std::move(cs));
}

template<typename ...TShaders>
void ShaderProgram::attachAndCompile(TShaders... shaders)
{
	std::vector<GLchar> rawLog;

	attach(std::forward<TShaders>(shaders)...);

	//
	glLinkProgram(m_program);
	checkGl();

	GLint linkInfoLog;
	glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &linkInfoLog);
	checkGl();

	if (linkInfoLog)
	{
		rawLog.resize(linkInfoLog);
		glGetProgramInfoLog(m_program, linkInfoLog, nullptr, rawLog.data());
		m_linkLog = std::string(rawLog.cbegin(), rawLog.cend());
#ifdef _DEBUG
	#if defined(_WINDOWS)
		OutputDebugStringA("Shader program link log:\n");
		OutputDebugStringA(m_linkLog.c_str());
		OutputDebugStringA("\n");
	#elif defined(ANDROID)
		__android_log_print(ANDROID_LOG_FATAL, "GLCPP", "Shader program link log\n%s", m_linkLog.c_str());
	#endif
#endif
		ensure(m_linkLog.empty());
	}

	GLint linkStatus;
	glGetProgramiv(m_program, GL_LINK_STATUS, &linkStatus);
	checkGl();
	ensure(linkStatus == GL_TRUE);

	//
	glValidateProgram(m_program);
	checkGl();

	GLint validateInfoLog;
	glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &validateInfoLog);
	checkGl();

	if (validateInfoLog)
	{
		rawLog.resize(validateInfoLog);
		glGetProgramInfoLog(m_program, validateInfoLog, nullptr, rawLog.data());
		m_validationLog = std::string(rawLog.cbegin(), rawLog.cend());
		ensure(m_validationLog.empty());
	}

	GLint validateStatus;
	glGetProgramiv(m_program, GL_VALIDATE_STATUS, &validateStatus);
	checkGl();
	ensure(validateStatus == GL_TRUE);

	m_shaders = std::vector<std::shared_ptr<ShaderBase>>({ std::forward<TShaders>(shaders)... });
}

void ShaderProgram::attach(const std::shared_ptr<VertexShader>& vs, const std::shared_ptr<FragmentShader>& fs)
{
	glAttachShader(m_program, vs->nativeHandle());
	checkGl();

	glAttachShader(m_program, fs->nativeHandle());
	checkGl();
}

void ShaderProgram::attach(const std::unique_ptr<ComputeShader>& cs)
{
	glAttachShader(m_program, cs->nativeHandle());
	checkGl();
}

GLint ShaderProgram::uniformLoc(std::string name) const noexcept
{
	if (auto it = std::find_if(m_uniformLocations.begin(), m_uniformLocations.end(),
		[&](const auto& pair)
	{
		return pair.first == name;
	}); it != m_uniformLocations.end())
	{
		return it->second;
	}
	GLint location = glGetUniformLocation(m_program, name.c_str());
	checkGl();

	ensure(location != -1);

	m_uniformLocations.emplace_back(std::move(name), location);
	return location;
}

GLint ShaderProgram::attribLoc(std::string name) const noexcept
{
	if (auto it = std::find_if(m_attributeLocations.begin(), m_attributeLocations.end(),
		[&](const auto& pair)
	{
		return pair.first == name;
	}); it != m_attributeLocations.end())
	{
		return it->second;
	}
	GLint location = glGetAttribLocation(m_program, name.c_str());
	checkGl();

	ensure(location != -1);

	m_attributeLocations.emplace_back(std::move(name), location);
	return location;
}

void ShaderProgram::setUniform(std::string name, GLuint scalar) noexcept
{
#ifdef _DEBUG
	GLint curProgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &curProgram);
	checkGl();
	ensure(curProgram == m_program);
#endif

	const auto loc = uniformLoc(std::move(name));
	glUniform1ui(loc, scalar);
	checkGl();
}
void ShaderProgram::setUniform(std::string name, GLint scalar) noexcept
{
#ifdef _DEBUG
	GLint curProgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &curProgram);
	checkGl();
	ensure(curProgram == m_program);
#endif

	const auto loc = uniformLoc(std::move(name));
	glUniform1i(loc, scalar);
	checkGl();
}
void ShaderProgram::setUniform(std::string name, GLfloat scalar) noexcept
{
#ifdef _DEBUG
	GLint curProgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &curProgram);
	checkGl();
	ensure(curProgram == m_program);
#endif

	const auto loc = uniformLoc(std::move(name));
	glUniform1f(loc, scalar);
	checkGl();
}
void ShaderProgram::setUniform(std::string name, GLfloat x, GLfloat y) noexcept
{
#ifdef _DEBUG
	GLint curProgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &curProgram);
	checkGl();
	ensure(curProgram == m_program);
#endif

	const auto loc = uniformLoc(std::move(name));
	glUniform2f(loc, x, y);
	checkGl();
}

void ShaderProgram::setUniform(std::string name, GLfloat x, GLfloat y, GLfloat z) noexcept
{
#ifdef _DEBUG
	GLint curProgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &curProgram);
	checkGl();
	ensure(curProgram == m_program);
#endif

	const auto loc = uniformLoc(std::move(name));
	glUniform3f(loc, x, y, z);
	checkGl();
}

void ShaderProgram::setUniformMat4(std::string name, GLfloat* mat4x4) noexcept
{
#ifdef _DEBUG
	GLint curProgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &curProgram);
	checkGl();
	ensure(curProgram == m_program);
#endif

	const auto loc = uniformLoc(std::move(name));
	glUniformMatrix4fv(loc, 1, GL_FALSE, mat4x4);
	checkGl();
}

}