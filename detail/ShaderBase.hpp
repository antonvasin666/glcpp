#pragma once

#include <string>

#include "opengl.hpp"

#define GLCPP_SHADER_CODE(...) #__VA_ARGS__

namespace glcpp
{

class ShaderBase
{
public:
	explicit ShaderBase(GLenum type);
	ShaderBase(const ShaderBase&) = delete;
	ShaderBase(ShaderBase&&) noexcept = delete;
	~ShaderBase();

	ShaderBase& operator= (const ShaderBase&) = delete;
	ShaderBase& operator= (ShaderBase&&) noexcept = delete;

	const std::string& compilationLog() const noexcept { return m_log; }
	GLuint nativeHandle() const noexcept { return m_shaderId; }

protected:
	const std::string& compile(const GLchar* src);

private:
	GLuint m_shaderId;
	std::string m_log;
};

}