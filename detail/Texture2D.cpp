#include <glcpp/Texture2D.hpp>

namespace glcpp
{

Texture2D::Texture2D() noexcept : UnmanagedTexture2D()
{
	glGenTextures(1, &m_texture);
	checkGl();
}

Texture2D::~Texture2D()
{
	glDeleteTextures(1, &m_texture);
	checkGl();
}

void Texture2D::allocate(GLsizei width, GLsizei height, GLint internalFormat, std::optional<TextureData> texture)
{
	if (width == m_width && height == m_height && internalFormat == m_internalFormat)
		return;

	UnmanagedTexture2D::bind(0, [&]
	{
		glTexStorage2D(GL_TEXTURE_2D, 1, internalFormat, width, height);
		checkGl();
		if (texture)
		{
			glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, texture->format, texture->type, texture->data);
			checkGl();
		}

		GLint status;
		glGetTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_IMMUTABLE_FORMAT, &status);
		ensure(status == GL_TRUE);
	});

	m_width = width;
	m_height = height;
	m_internalFormat = internalFormat;
}

}