#ifdef _DEBUG
	#if defined(_WINDOWS)
		#include <Windows.h>
	#elif defined(ANDROID)
		#include <android/log.h>
	#endif
#endif

#include <vector>

#include "contracts.hpp"

#include "ShaderBase.hpp"

namespace glcpp
{

ShaderBase::ShaderBase(GLenum type)
{
	m_shaderId = glCreateShader(type);
	checkGl();
}

ShaderBase::~ShaderBase()
{
	glDeleteShader(m_shaderId);
	checkGl();
}

const std::string& ShaderBase::compile(const GLchar* src)
{
	ensure(src != nullptr);

	const GLchar* const lines[] = { src };
	glShaderSource(m_shaderId, 1, lines, nullptr);
	checkGl();

	glCompileShader(m_shaderId);
	checkGl();

	GLint logSize;
	glGetShaderiv(m_shaderId, GL_INFO_LOG_LENGTH, &logSize);
	checkGl();

	if (logSize)
	{
		std::vector<GLchar> rawLog(logSize, GLchar{ 0 });
		glGetShaderInfoLog(m_shaderId, logSize, nullptr, rawLog.data());
		checkGl();
		m_log = std::string(rawLog.cbegin(), rawLog.cend());
#ifdef _DEBUG
    #if defined(_WINDOWS)
		OutputDebugStringA("Shader compilation log:\n");
		OutputDebugStringA(m_log.c_str());
		OutputDebugStringA("\n");
    #elif defined(ANDROID)
		__android_log_print(ANDROID_LOG_FATAL, "GLCPP", "Compiling shader:\n%s", src);
		__android_log_print(ANDROID_LOG_FATAL, "GLCPP", "Compilation log:\n%s", m_log.c_str());
    #endif
#endif
		ensure(m_log.empty());
	}

	GLint compileStatus;
	glGetShaderiv(m_shaderId, GL_COMPILE_STATUS, &compileStatus);
	checkGl();
	ensure(compileStatus == GL_TRUE);

	return compilationLog();
}

}